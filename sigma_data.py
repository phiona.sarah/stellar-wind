#this converts the data into lists that we can then use
data = []
for line in open("H2 data"):
    fields = [float(s) for s in line.split()]
    
    data.append(fields)
#print(data)

#gets the data between certian numbers on the list
def interpolated_data (data,xvalue):
    
    for w in range(len(data)):
        
        if data[w][0] >= xvalue:
             break
       
    if w==0: 
        w=1
    data[w-1],data[w]  
    
    
    
    return getdata(data[w-1][0],data[w][0],data[w-1][6],data[w][6],xvalue)


def interpolated_weighteddata (data,xvalue):
    
    for w in range(len(data)):
        
        if data[w][0] >= xvalue:
             break
       
    if w==0: 
        w=1
    data[w-1],data[w]  
    ya,yb = weighted(data[w-1][1],data[w][1],data[w-1][2],data[w][2],data[w-1][6],data[w][6])
    
    return getdata(data[w-1][0],data[w][0],ya,yb,xvalue) #gets weighted data 
    print (yc)
    
    
    
#x1 upper coherent scatering
#x2 upper incoherent scatering
#x3 upper total scattering
#x4 lower coherent scatering
#x5 lower incoherent scatering
#x6 lower total scattering
#this gets the weighted data 
def weighted(x1,x2,x3,x4,x5,x6):
    
    ya = (x1+x3)/x5
    yb = (x2+x4)/x6
    return ya,yb



#function to interpolate 
#xc would be the xvalue given 
# xb would be the 0 value of the list for the lower number
# xa would be the 0 value of the list for the uper number
# yb would be the 6 value of the list for the lower number
# ya would be the 6 value of the list for the upper number
def getdata(xa,xb,ya,yb,xc):
    
    m = (ya - yb) / (xa - xb)
    yc = (xc - xb) * m + yb
    return yc



#def makeHistogram (sigma):
import math
import numpy as np 
import matplotlib
import matplotlib.pyplot as plt
from random import randrange
def makeHistogram (sigma):
    #sigma=3.355E-01 #cm2/g
   
    rho=0.07 #g/cm3
    print(sigma)
    return sigma

    k=rho*sigma
    r = np.random.random(10000) 
    x = np.random.exponential(1/k,10000) 
#print(r)
#x=np.log(r*(-k)+k)/(-k)


    #return interpolated_weighteddata(data,xvalue)
    count, bins, ignored = plt.hist(x, 20, density=True)
    plt.title('Probability Distribution')
    plt.xlabel('Distance Traveled (cm)')
    plt.ylabel('Photon Energy (MeV)')
    
    plt.show()
    





