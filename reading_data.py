import stellar_wind as sw
import sigma_data as sd
import math 
import numpy as np
import matplotlib.pyplot as plt

def read_data(theta_observer,phi_observer):
    #phi_observer=np.pi/2
    #theta_observer=np.pi/4
    #creat empty lists
    number_in_cone=0
    number_of_escaped=0
    number_of_absorbed=0
    number_of_scattered=0
    number_escaped_first=0
    v_observer=np.array([np.cos(phi_observer)*np.sin(theta_observer),np.sin(phi_observer)*np.sin(theta_observer),np.cos(phi_observer)])
    #print(v_observer)
    primary_energy = None
    last_index = None
    
    data = []
    status_of_photon=[]
    for line in open("data/tracking.txt"):
        line=line.strip()
        fields=line.split()
        
    
        if len(line)==0 or line[0]=='#':
            continue 
        counter = int(fields[0])
        energy = float(fields[1])
        status_of_photon.append(float(fields[13]))
        if counter != last_index:
            
            primary_energy =  energy
            last_index = counter
            if float(fields[13])==3.0:
                number_escaped_first+=1
        if float(fields[13])==2.0:
            number_of_absorbed+=1
        if float(fields[13])==1.0:
            number_of_scattered+=1
        if float(fields[13])==3.0:
            number_of_escaped+=1
            fields = [float(x) for x in fields]
        
            
            v=np.array([fields[5],fields[6],fields[7]])
            v /= sw.length(v)
#picking what we want theta and phi to be inbetween so that the photons are in the cone
            #print(v)
            test_in_cone=np.arccos(np.dot(v,v_observer) )
            #print(test_in_cone*180/np.pi)
            cone_angle=10
            
            
            if test_in_cone < cone_angle*np.pi/180 and test_in_cone > 0:
                number_in_cone+=1
             #calculating stokes paremters 
                weight=fields[12]
                
                
                z_hat=np.array([0,0,1] , dtype = float) #where the star is in the z direction
                pol_vector=np.array([fields[8],fields[9],fields[10]])
                pol_fraction=fields[11]
                x_tilde=np.cross(z_hat,v_observer)/sw.length(np.cross(z_hat,v_observer))
                y_tilde=np.cross(v_observer,x_tilde)/sw.length(np.cross(v_observer,x_tilde))
            
                psi_observer=np.arccos(np.dot(pol_vector,x_tilde)/(sw.length(pol_vector)*sw.length(x_tilde)))
                #print(np.dot(pol_vector,x_tilde)/(sw.length(pol_vector)*sw.length(x_tilde)))
                I_stoke=weight
                Q_stoke=weight*pol_fraction*np.cos(2*psi_observer)
                U_stoke=weight*pol_fraction*np.sin(2*psi_observer) 
                iqu=np.array([I_stoke,Q_stoke,U_stoke])
                data.append([counter,primary_energy,energy,v,weight,pol_vector,pol_fraction,psi_observer,I_stoke,Q_stoke,U_stoke,iqu]) 
                
                    #number_of_escaped,number_of_absorbed,number_of_scattered,stokesI,stokesU,stokesQ 
                    
    return  data,number_of_absorbed,number_of_scattered,number_of_escaped,number_escaped_first,status_of_photon,number_in_cone           
    
    