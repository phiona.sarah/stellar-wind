
import sigma_data as sd
import math 
import numpy as np
import matplotlib.pyplot as plt
r0=2.818E-15 #constant

#to bring in data to use
data = []
for line in open("H2 data"):
    fields = [float(s) for s in line.split()]
    
    data.append(fields)
    
#cash-karp paramters for fifth-order runga-kutta integration 
aS=np.array([0,1/5,3/10,3/5,1,7/8])
cS=np.array([37/378,0,250/621,125/594,0,512/1771])
cSstar=np.array([2825/27648,0,18575/48384,13525/55296,277/14336,1/4])

def densityAndVelocity (v0,v_terminal,r_star,m_wind,r):
    v0=v0*100000  #cm/second 
    v_terminal=v_terminal*100000 # terminal velocity cm/second 
    m_wind=m_wind*1.989E33/3.154E7
    #print(r)
    v=np.where(r>r_star,v0+(v_terminal-v0)*(1-(r_star/r)),v0)
    #v=v0+(v_terminal-v0)*(1-(r_star/r)) #equation 1
    
    #if np.any(r) <= np.all(r_star):
       # v=v0
    #else:
        #v=v0+(v_terminal-v_not)*(1-(r_star/r)) #equation 1
    rho = m_wind/(4*math.pi*r**2*v) #equation 2
    return rho

def LambdaX(rho,sigma):
    lambda1=1/(rho*sigma) #equation 4
    
    #print (lambda1)
    return lambda1

def length (p):
    length=math.sqrt(p[0]**2+p[1]**2+p[2]**2)
    return length

def getv():
    theta=math.acos(2*np.random.random()-1) #theta is from -1 to 1
    phi=2*math.pi*np.random.random() #phi is from 0 to 2pi
    
    lv=3E10
    v=np.array([lv*math.sin(theta)*math.cos(phi),lv*math.sin(theta)*math.sin(phi),lv*math.cos(theta)])
    return v

#equation 33 
def compton_formula(THETA,epsilon):


    
    epsilon_prime=epsilon/(1+epsilon*(1-np.cos(THETA)))
        
    return epsilon_prime

#equation 33 taking in a list of thetas to print a list of epsilone primes
def compton_effect(THETA,epsilon):


    for i in THETA:
        epsilon_prime=epsilon/1+(epsilon)*(1-np.cos(THETA))
        
    return epsilon_prime

def fano(THETA,epsilon_prime,epsilon,IQU):
    r0=2.818E-15
    #equation 36
    #d_sigma_d_omega=1/2*(r0**2)*((epsilon/epsilonprim)**2)*((epsilon/epsilonprim)+(epsilonprime/epsilon)-2*math.sin(THETA)**2*math.sin(psi)**2)
    Tkn_matrix=np.array([[1+math.cos(THETA)**2+(epsilon-epsilon_prime)*(1-math.cos(THETA)) , math.sin(THETA)**2 , 0],[math.sin(THETA)**2,1+math.cos(THETA)**2,0],[0,0,2*math.cos(THETA)]])
    #print(Tkn_matrix)                                                      
    Tkn=1/2*(r0**2)*((epsilon/epsilon_prime)**2)*Tkn_matrix
    return np.dot(Tkn,IQU)

def pol_fraction(IQU):
    #equation 27
    return math.sqrt(IQU.item(1)**2+IQU.item(2)**2)/IQU.item(0)

def weight_prime(energy,THETA,psi,p):
    me=.511 #MeV
    epsilon=energy/(me)
    epsilon_prime=compton_formula(THETA,epsilon)
    
    I=1
    Q=p*math.cos(2*psi)
    U=-p*math.sin(2*psi)
    IQU=np.array([I,Q,U])
    
    IQU_prime=fano(THETA,epsilon_prime,epsilon,IQU)
    
    #equation 37
    return (4*math.pi*IQU_prime[0])/sigma_kn(epsilon)


#equation 38
def sigma_kn(epsilon):
    return 2*math.pi*r0**2*\
    (
        (1+epsilon)/(epsilon**2)*\
            (
                2*(1+epsilon)/(1+2*epsilon)-\
                math.log(1+2*epsilon)/epsilon
            )+\
        math.log(1+2*epsilon)/(2*epsilon)-(1+3*epsilon)/(1+2*epsilon)**2
    )

def compton_graphing(theta,energy):
    
    epsilon=energy/.511
    epsilon_prime=compton_formula(theta,epsilon)
    IQU_prime=fano(theta,epsilon_prime,epsilon,np.array([1,0,0]))
    
    
    #print(theta,IQU_prime)
    
    return pol_fraction(IQU_prime)


#compton scattering 
def compton_scattering (v,f,p,energy,vnew=None):
    if vnew is None:
        vnew=getv()
    c=3E8
    me=.511 #MeV
    
    v_normalized=v/length(v)
    #equation 34
    THETA=math.acos(np.dot(v,vnew)/(length(v)*length(vnew)))
    #equation 33
    #energy is in units of MeV
    epsilon=energy/(me)
    epsilon_prime=compton_formula(THETA,epsilon)
    
    #equation 23
    if p>=0:
        e1=f
    else:
        e1=np.cross(np.cross(vnew,v),v)/length(np.cross(vnew,v))
    e2=np.cross(v_normalized,e1)
    e3=v_normalized
    #print("e1=",e1)
    #print("e2=",e2)
    #print("e3=",e3)
    #equation 24
    b_perpendicular=np.cross(v,vnew)/length(np.cross(v,vnew))
    b_parrallel=np.cross(b_perpendicular,vnew)
    b3=vnew/length(vnew)
    
    psi=np.arccos(np.dot(e1,b_perpendicular)) #caluclautes psi
    
    #equation 25
    I=1
    Q=p*math.cos(2*psi)
    U=-p*math.sin(2*psi)
    IQU=np.array([I,Q,U])
    #print("stokes=", IQU)
      
    #equation 26
    IQU_prime=fano(THETA,epsilon_prime,epsilon,IQU)
    #print("stokes_prime=",IQU_prime)
    
    #equation 27
    p_prime=pol_fraction(IQU_prime)
    #equation 28
    psi_prime=.5*np.arctan2(-IQU_prime.item(2),IQU_prime.item(1))
    
        
    #equation 29
    #f_prime=b_perpendicular*math.cos(psi_prime)+np.cross(b3,b_perpendicular)*math.sin(psi_prime)
    f_prime=b_perpendicular*math.cos(psi_prime)+np.sign(np.dot(b_parrallel,e2))*b_parrallel*math.sin(psi_prime)
    #equation 38
    sigma_kn=(2*math.pi*r0**2)*(1+epsilon)/(epsilon**2)*((2*(1-epsilon)/(1+2*epsilon)-math.log(1+2*epsilon)/epsilon))+(math.log(1+2*epsilon)/2*epsilon)-(1+3*epsilon)/(1+2*epsilon)**2
    #equation 37
    weight_prime=(4*math.pi*IQU_prime[0])/sigma_kn
    
    return f_prime/length(f_prime),epsilon_prime*me,p_prime,vnew,weight_prime,psi,psi_prime,THETA

def Integrating(h,n_lambda,v,x,sigma,epsilon):
#h=.033 #starting step size
    
    integral=0.0 
    oldintegral=0.0
    s=.95 #saftey factor
    #epsilon=1E-6 #desired relative accuracy
     #x starting point
    star_radius=1E12
    #lv=3E10
    #v=np.array([lv*math.cos(phi),lv*math.sin(phi),0]) #velocity vector
    
    #n_lambda=.00017098 #number of mean free paths
    lv=length(v) #length of v
    
    xpoints=[]
    integralold=[]
    integralnew=[]
    listdelta=[]
    hlist=[]
    alambda=[]
    blist=[]
    status=1
    while abs((integral - n_lambda)/n_lambda) > 1e-3:
         
        points=[x+a*h*v for a in aS] #calculates diffrent vectors for each a
        
        radii=np.array([length(p) for p in points]) #calculates the length of each vector units cm
        
        rho=densityAndVelocity(100,1000,1E12,1E-8,radii)
        
        invlambda=1/LambdaX(rho,sigma)
        
        
        #print("sigma=",sigma)
        #print("rho=",rho)
        #print("invlambda=",invlambda)
        ks=lv*h*invlambda 
        
        delta=abs(np.dot((cS-cSstar),ks))
        
        
        
        newintegral=integral+ np.dot(ks,cS) # eqaution 14
        
        
    
        delta0=epsilon*newintegral #equation 18
    
        
    #caluclates how h should be changed
        
        if delta0 >= delta:
            if newintegral > n_lambda:
                h*=.5
                continue 
            if delta < .001*delta0:
                
                delta = .001*delta0
           
            #print(h,'=before')    
            h=h*s*(delta0/delta)**(1/5)
            #print(h,'=after',s,delta0,delta)
        else :
            #print(rho,points,radii)
            h=h*s*(delta0/delta)**(1/4)
            #print(h,'=after after',s,delta0,delta)
            continue
    #ldx=h*length(v)
        xpoints.append(length(x))
        
        
        integral=newintegral
        integralnew.append(newintegral)
        listdelta.append(delta)
        hlist.append(h)
        
        x=x+h*v
        #print(x,h,v,integral)
    #oldintegral += ldx/LambdaX(density(velocity(100,1000,1E12,1E-8,length(x)),1E-8,length(x)))
        #a=np.array(wind.LambdaX(sd.density(100,1000,1E12,1E-8,sd.length(x)),1E-8,sd.length(x))))
    #integralold.append(oldintegral)
        #alambda.append(a)
    
    
    
        #b=np.array(sd.length(x0-x))
        #blist.append(b)
    #breaks the loop if the photon does not interact 
        #if length(x) <=1E12:
            #print("hit star")
            #break 
        #if length(x) > 1E13:
           # print("escaped")
            #break
    #print(integralnew) 
         
        
        status=1 #1 means scattered   
        if length(x) < star_radius:
            status= 2 #"hit star"
            #print("hit star")
            break
        elif length(x) > 1e14: 
            status= 3 #"escaped"
            #print("escaped")
            break
        
            
    return integralnew,x,status
 

#generate one photon at a time track this photon and record the information 
#contiune with process n times 
#tack one photon then put this into a larger for loop to track multiple photons 
def tracking (counter,energy,starting_point,v,out_file=None):

    
        
#energy = 1E-2 #intial energy
#starting_point = 1.5E12*np.array([1,0,0])
    x = starting_point #x is where the photon is
    pol_fraction = 0 #intial polarization fraction
    pol_vector = np.array([1,0,0]) 
    weight=1
#v = np.random.random()*np.array([1,1,0]) #v is the varible name for the random direction 
# should i change this to a while loop and have it run till staus is not 1
    for index in range(5):
    
    
        
        
        sigma = sd.interpolated_data(data,energy)
        
    
        n_lambda =  -math.log(np.random.random())
        h = .033 #intail step length
        integralnew,x,status = Integrating(h,n_lambda,v,x,sigma,energy)
        if out_file is not None:
            
            out_file.write('%d %g %g %g %g %g %g %g %g %g %g %g %g %d \n'%(counter,energy,x[0],x[1],x[2],v[0],v[1],v[2],pol_vector[0],pol_vector[1],pol_vector[2],pol_fraction,weight,status))
        if status == 1:
            pol_vector,energy,pol_fraction,v,w_prime,psi,psi_prime,theta = compton_scattering(v,pol_vector,pol_fraction,energy,vnew=None)
            
            weight=weight*w_prime
            
        else :
            break 
        
    
    #energy_list.append(energy)
    #v_list.append(v)
    #pol_fraction_list.append(pol_fraction)
    
    
    
    return energy,x,v, weight 
    
    













